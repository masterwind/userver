// ==UserScript==
// @name         uServer
// @description  Вспомогательная панель саппортера
// @namespace    userver-dma-masterwind
// @author       dma, masterwind
// @version      22.8.10
// @updateURL    https://userver.netlify.app/userver.user.js
// @downloadURL  https://userver.netlify.app/userver.user.js
// @supportURL   https://bitbucket.org/masterwind/userver/issues
// @include      *
// @exclude      *//sp.ucoz.ru/*
// @require      https://utils-masterwind.netlify.app/utils.js?v=1619524940146
// @grant        none
// @noframes
// ==/UserScript==


((stp, userver, template ) => {
	'use strict';
stp.debug && console.clear();
stp.debug && console.log( `window.utils`, window.utils );
	if (!window.utils) return false;

stp.debug && console.log( `location.pathname.includes('admin.cgi')`, location.pathname.includes('admin.cgi') );
stp.debug && console.log( `location.search.includes('a=moders')`,    location.search.includes('a=moders') );
	if (location.pathname.includes('admin.cgi') || location.search.includes('a=moders')) return false;

stp.debug && console.log( `document.contentType.includes('html')`, document.contentType.includes('html') );
	if (document.contentType.includes('html')) {
		userver.getOwnerData(); // get owner data
		utils.css.setLink('https://userver.netlify.app/userver.css'); // set styles
	}
})(
	window.stp = {
		// debug     : 1,
		position  : localStorage.userverSidebarPosition || 'right',  // left | right - TODO toggle position
		whoisLink : 'https://whois.vu/?q=',
		searchers : [
			{ target:'_blank', url:'https://www.google.ru/search?q={{domain}}',                        title:'в Google',                                                      },
			{ target:'_blank', url:'https://yandex.ru/search/?text={{domain}}',                        title:'в Yandex',                                                      },
			{ target:'_blank', url:'https://webcache.googleusercontent.com/search?q=cache:{{domain}}', title:'в кеше Google',                                                 },
			{ target:'_blank', url:'https://web.archive.org/web/*/{{domain}}',                         title:'в Вебархиве',                                                   },
			{ target:'_blank', url:'https://yandex.ru/infected?url={{domain}}',                        title:'в Я.Malware',                                                   },
			{ target:'_blank', url:'https://www.google.ru/safebrowsing/diagnostic?site={{domain}}',    title:'в G.safebrowsing',                                              },
			{                  url:'javascript:;',                                                     title:'whois',                  onclick:"userver.whoisIframe()",       },
			{ target:'_blank', url:'https://virustotal.com/ru/domain/{{domain}}/information/',         title:'VirusTotal domain info',                                        },
			{ target:'_blank', url:'https://virustotal.com/ru/url/submission/?url={{domain}}',         title:'VirusTotal site check',                                         },
			{ target:'_blank', url:'{{findModerListingURL}}',                                          title:'Письма в саппорт',       title:'Найти все обращения в саппорт', },
		],
	},
	window.userver = {
		// whois frame
		whoisIframe : (href = stp.whoisLink + userver.ownerData.domain) => $.fancybox ? $.fancybox({ type:"iframe", href, wrapCSS:'sptool-whois' }) : window.open(href),

		// init userver
		init : ownerData => {
			// check if domain
			(ownerData.domain = location.host == ownerData.chost ? ownerData.chost : location.host);
			// set link url to moder dialog
			(ownerData.user = utils.convert.site2user(ownerData.chost)) && (ownerData.dom = ownerData.user[0])
				&& (ownerData.findModerListingURL = `https://${ ownerData.shost }/panel/?a=moders&l=f&q=${ ownerData.user.slice(1) }&t=1&dom=${ ownerData.dom }`);
			// new CP
			ownerData.newcp = ownerData.cp == 2;  // check if new CP by default
			ownerData.onNewCp = [ ...document.querySelectorAll('link') ].filter(item => item.href.includes('/panel-v2/')).length;  // check if new CP is current page
			// update expanded status
			ownerData.expanded = +localStorage.sptool_open && 'expanded' || '';
			// update position
			ownerData.position = stp.position;
			// prepare data
			[ ownerData.shop, ownerData.forum ] = [ !!ownerData.servs.shop, !!ownerData.servs.forum ];

			// set base styles if needed
			ownerData.needStyle && utils.css.setLink(`https://${ ownerData.shost }/.s/src/layer1.min.css`)  // set styles

stp.debug && console.log(`userver.init::ownerData`, ownerData );
			// set uServer sidebar & widget
			template.setUserver(ownerData);
			// set find-In-aLog link
			!!document.querySelector('#uerror.blocked') && document.querySelector('#error-descr').insertAdjacentHTML('afterbegin',
				`<a class="alogsp" href="http://alog.ucozmedia.com/inner/?q=${ ownerData.chost }">Найти сайт в Action Log</a>`);
			// cookie policy repadding
			window.cookie_policy && cookie_policy.classList.add('repadded');
			// set owner data
			userver.ownerData = ownerData;
		},

		getCommentServer : parent => {
			let commentData = document.createNodeIterator(document, NodeFilter.SHOW_COMMENT, node => +(/\(s\d+\s?\)/.test(node.data))).nextNode()?.data;
			return commentData?.substring(commentData.indexOf('(') + 1, commentData.indexOf(')')).trim()
		},

		getErrorData : response => {
stp.debug && console.log(`getErrorData::response`, response);
			switch (response.status) {
				case 403:
				case 404: return { server:userver.getCommentServer(document), chost:location.host, servs:{}, needStyle:true };
				default : return { server:'s0' }
			}
		},

		// get owner data
		getOwnerData : () => fetch && fetch(location.origin + '/index/showmeowner', { method:'POST' })
			// .then(r => r.ok && r.json() || console.log('response', r), e => console.log(e))
			.then(r => {
stp.debug && console.log(`getOwnerData::response`, r);
				return r.ok ? r.json() : userver.getErrorData(r)
			})
			// init userver
			.then((data = {}) => {
stp.debug && console.log(`getOwnerData::data`, data);
				data.server && (data.shost = data.server + ({ uweb:'.uweb.ru' }[ data.cluster ] || '.ucoz.net')) && userver.init(data)
			})
			// .catch((error, r) => console.log('catch::error', error, r))
		,

		togglePosition : () => {
			// update position
			let positionSwitch = { left:'right', right:'left' };
			userver.ownerData.position = stp.position = positionSwitch[ userver.ownerData.position || stp.position ];
			localStorage.setItem('userverSidebarPosition', userver.ownerData.position);

			// change widget position
			udraggable.classList.toggle(userver.ownerData.position);
			udraggable.classList.toggle(positionSwitch[ userver.ownerData.position ]);
		},

		toggleSidebar : (toggler, tClass = 'expanded') => {
			toggler.classList.toggle(tClass);
			sptools.classList.toggle(tClass);
			localStorage.setItem('sptool_open', +!+localStorage.sptool_open);
		},
	},
	window.template = {
		widget : `<div id=udraggable class="{{position}} u-hidden ??onNewCp=from-new-cp??">
			<u onclick="userver.togglePosition();"></u>
			<a class="server-num" target="_blank" href="https://{{shost}}/cgi/uAdmin/admin.cgi?user={{chost}}&l=fu&r=0" title="Найти сайт в админке">{{server}}</a>
			<i id="panel" onclick="utils.copyToClipboard(this.textContent); this.style.color='blue';" title="Копировать адрес">{{chost}}</i>??vip=<b>VIP</b>??
			<a href="javascript:;" onclick="utils.copyToClipboard(this.dataset.email); this.style.color='blue';" title="Копировать email" class="owner-email" data-email="{{email}}"></a>
			??newcp=<span class="newcpflag">v2</span>??
			<i id="sptoggle" class="toggle-sidebar {{expanded}}" onclick="userver.toggleSidebar(this);"></i>
		</div>`,
		sidebarPages : `
		<h3>Cтраницы</h3>
		<ul>
			<li><a href="/index/1${ window.uCoz && '0">Выход с сайта' || '">Страница входа' }</a>
			<li><a href="/index/8">Моя страница</a>
			<li><a href="/pda/">PDA</a>
			<li><a href="/index/53">Выход с PDA</a>
			??shop=<li><a href="/shop/invoices">Управление заказами</a>??
			<li><a target="_blank" href="/robots.txt">robots.txt</a>
			<li><a target="_blank" href="/sitemap.xml">sitemap.xml</a>
			??shop=<li><a target="_blank" href="/sitemap-shop.xml">sitemap-shop.xml</a>??
			??forum=<li><a target="_blank" href="/sitemap-forum.xml">sitemap-forum.xml</a>??
		</ul>`,
		sidebar : `<div id=sptools class="u-menu myWinTD1 {{expanded}} u-hidden u-user_menu-popover ">
			<img src="https://logo.ucoz.com/LOGO.svg" width=32 alt="favicon" />
			<h3>Модули</h3>
			<ul>
				<li><a href="/">Главная</a>
				{{modules}}
			</ul>
			{{sidebarPages}}
			<h3>Поиск сайта</h3>
			<ul>??searchers={{searchers}}??</ul>
		</div>`,
		button : '<li><a href={{url}} ??target=target={{target}}?? ??title=title="{{title}}"?? ??onclick=onclick="{{onclick}}"?? >{{title}}</a>',
		setUserver : data => {
			// prepare modules
			data.modules = Object.keys(data.servs).sort().map(module => !/mail|stat|feedsya|crosspost|feedsya|site|poll/.test(module)
				&& utils.template.render(template.button, data.servs[ module ]) || undefined).join('');

			// prepare search links
			data.searchers = stp.searchers.map(search => utils.template.render(template.button, search)).join('');

			// update sidebar pages
			if ((new Map(Object.entries(data.servs))).size) data.sidebarPages = utils.template.render(template.sidebarPages, data);

			// set content
			document.body.insertAdjacentHTML('beforeend',
				// set widget
				utils.template.render(template.widget, data)
				// set sidebar
				+ utils.template.render(utils.template.render(template.sidebar, data), data)
			)
		},
	}
);
